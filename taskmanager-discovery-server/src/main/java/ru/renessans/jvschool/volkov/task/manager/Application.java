package ru.renessans.jvschool.volkov.task.manager;

import org.jetbrains.annotations.Nullable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class Application {

    public static void main(@Nullable final String[] args) {
        SpringApplication.run(Application.class, args);
    }

}