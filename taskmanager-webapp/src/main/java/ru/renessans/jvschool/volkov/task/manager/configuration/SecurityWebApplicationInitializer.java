package ru.renessans.jvschool.volkov.task.manager.configuration;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public final class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {
}