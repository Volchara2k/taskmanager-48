package ru.renessans.jvschool.volkov.task.manager.exception.invalid.config;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidDriverException extends AbstractException {

    @NotNull
    private static final String EMPTY_DRIVER = "Ошибка! Параметр \"драйвер подключения\" отсутствует!\n";

    public InvalidDriverException() {
        super(EMPTY_DRIVER);
    }

}