package ru.renessans.jvschool.volkov.task.manager.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;

import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Date;

@Data
@XmlType
@SuperBuilder
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "All details about the time frame")
public final class TimeFrameDTO implements Serializable {

    @NotNull
    private static final String DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

    @NotNull
    private static final String TIMEZONE_PATTERN = "Europe/Moscow";

    @Nullable
    @JsonFormat(
            shape = JsonFormat.Shape.STRING,
            pattern = DATE_TIME_PATTERN,
            timezone = TIMEZONE_PATTERN
    )
    @ApiModelProperty(
            value = "Date the task was created",
            name = "creationDate",
            example = "2021-03-01 13:52:41"
    )
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date creationDate;

    @Nullable
    @JsonFormat(
            shape = JsonFormat.Shape.STRING,
            pattern = DATE_TIME_PATTERN,
            timezone = TIMEZONE_PATTERN
    )
    @ApiModelProperty(
            value = "Date the task was started",
            example = "2021-03-01 13:52:41",
            name = "startDate"
    )
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date startDate;

    @Nullable
    @JsonFormat(
            shape = JsonFormat.Shape.STRING,
            pattern = DATE_TIME_PATTERN,
            timezone = TIMEZONE_PATTERN
    )
    @ApiModelProperty(
            value = "Date the task was ended",
            example = "2021-03-01 13:52:41",
            name = "endDate"
    )
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date endDate;

}