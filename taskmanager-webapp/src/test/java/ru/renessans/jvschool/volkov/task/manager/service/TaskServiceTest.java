package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserTaskService;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataTaskProvider;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataUserProvider;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.entity.Task;
import ru.renessans.jvschool.volkov.task.manager.model.entity.User;

import java.util.Collection;
import java.util.UUID;

@Setter
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
@Category({PositiveImplementation.class, ServiceImplementation.class})
public class TaskServiceTest {

    @NotNull
    @Autowired
    private IUserTaskService userTaskService;

    @NotNull
    @Autowired
    private IUserService userService;

    @Test
    public void testAdd() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Task task = CaseDataTaskProvider.createTask();
        Assert.assertNotNull(task);
        task.setUserId(user.getId());
        task.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);

        @NotNull final Task addTaskResponse = this.userTaskService.addUserOwner(user.getId(), task.getTitle(), task.getDescription());
        Assert.assertNotNull(addTaskResponse);
        Assert.assertEquals(task.getUserId(), addTaskResponse.getUserId());
        Assert.assertEquals(task.getTitle(), addTaskResponse.getTitle());
        Assert.assertEquals(task.getDescription(), addTaskResponse.getDescription());
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Task task = CaseDataTaskProvider.createTask();
        Assert.assertNotNull(task);
        task.setUserId(user.getId());
        task.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Task addTaskResponse = this.userTaskService.addUserOwner(user.getId(), task.getTitle(), task.getDescription());
        Assert.assertNotNull(addTaskResponse);

        @NotNull final String newData = UUID.randomUUID().toString();
        Assert.assertNotNull(newData);
        @Nullable final Task updateTaskResponse = this.userTaskService.updateUserOwnerByIndex(saveUserResponse.getId(), 0, newData, newData);
        Assert.assertNotNull(updateTaskResponse);
        Assert.assertEquals(addTaskResponse.getId(), updateTaskResponse.getId());
        Assert.assertEquals(addTaskResponse.getUserId(), updateTaskResponse.getUserId());
        Assert.assertEquals(newData, updateTaskResponse.getTitle());
        Assert.assertEquals(newData, updateTaskResponse.getDescription());
    }

    @Test
    public void testUpdateById() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Task task = CaseDataTaskProvider.createTask();
        Assert.assertNotNull(task);
        task.setUserId(user.getId());
        task.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Task addTaskResponse = this.userTaskService.addUserOwner(user.getId(), task.getTitle(), task.getDescription());
        Assert.assertNotNull(addTaskResponse);

        @NotNull final String newData = UUID.randomUUID().toString();
        Assert.assertNotNull(newData);
        @Nullable final Task updateTaskResponse = this.userTaskService.updateUserOwnerById(saveUserResponse.getId(), addTaskResponse.getId(), newData, newData);
        Assert.assertNotNull(updateTaskResponse);
        Assert.assertEquals(addTaskResponse.getId(), updateTaskResponse.getId());
        Assert.assertEquals(addTaskResponse.getUserId(), updateTaskResponse.getUserId());
        Assert.assertEquals(newData, updateTaskResponse.getTitle());
        Assert.assertEquals(newData, updateTaskResponse.getDescription());
    }

    @Test
    public void testDeleteByIndex() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Task task = CaseDataTaskProvider.createTask();
        Assert.assertNotNull(task);
        task.setUserId(user.getId());
        task.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Task addTaskResponse = this.userTaskService.addUserOwner(user.getId(), task.getTitle(), task.getDescription());
        Assert.assertNotNull(addTaskResponse);

        final int deleteTaskResponse = this.userTaskService.deleteUserOwnerByIndex(saveUserResponse.getId(), 0);
        Assert.assertEquals(1, deleteTaskResponse);
    }

    @Test
    public void testDeleteById() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Task task = CaseDataTaskProvider.createTask();
        Assert.assertNotNull(task);
        task.setUserId(user.getId());
        task.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Task addTaskResponse = this.userTaskService.addUserOwner(user.getId(), task.getTitle(), task.getDescription());
        Assert.assertNotNull(addTaskResponse);

        final int deleteTaskResponse = this.userTaskService.deleteUserOwnerById(saveUserResponse.getId(), addTaskResponse.getId());
        Assert.assertEquals(1, deleteTaskResponse);
    }

    @Test
    public void testDeleteByTitle() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Task task = CaseDataTaskProvider.createTask();
        Assert.assertNotNull(task);
        task.setUserId(user.getId());
        task.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Task addTaskResponse = this.userTaskService.addUserOwner(user.getId(), task.getTitle(), task.getDescription());
        Assert.assertNotNull(addTaskResponse);

        final int deleteTaskResponse = this.userTaskService.deleteUserOwnerByTitle(saveUserResponse.getId(), addTaskResponse.getTitle());
        Assert.assertEquals(1, deleteTaskResponse);
    }

    @Test
    public void testDeleteAll() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Task task = CaseDataTaskProvider.createTask();
        Assert.assertNotNull(task);
        task.setUserId(user.getId());
        task.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Task addTaskResponse = this.userTaskService.addUserOwner(user.getId(), task.getTitle(), task.getDescription());
        Assert.assertNotNull(addTaskResponse);

        final int deleteTasksResponse = this.userTaskService.deleteUserOwnerAll(saveUserResponse.getId());
        Assert.assertEquals(1, deleteTasksResponse);
    }

    @Test
    public void testGetByIndex() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Task task = CaseDataTaskProvider.createTask();
        Assert.assertNotNull(task);
        task.setUserId(user.getId());
        task.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Task addTaskResponse = this.userTaskService.addUserOwner(user.getId(), task.getTitle(), task.getDescription());
        Assert.assertNotNull(addTaskResponse);

        @Nullable final Task taskResponse = this.userTaskService.getUserOwnerByIndex(saveUserResponse.getId(), 0);
        Assert.assertNotNull(taskResponse);
        Assert.assertEquals(addTaskResponse.getId(), taskResponse.getId());
        Assert.assertEquals(addTaskResponse.getUserId(), taskResponse.getUserId());
        Assert.assertEquals(addTaskResponse.getTitle(), taskResponse.getTitle());
        Assert.assertEquals(addTaskResponse.getDescription(), taskResponse.getDescription());
    }

    @Test
    public void testGetById() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Task task = CaseDataTaskProvider.createTask();
        Assert.assertNotNull(task);
        task.setUserId(user.getId());
        task.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Task addTaskResponse = this.userTaskService.addUserOwner(user.getId(), task.getTitle(), task.getDescription());
        Assert.assertNotNull(addTaskResponse);

        @Nullable final Task taskResponse = this.userTaskService.getUserOwnerById(saveUserResponse.getId(), addTaskResponse.getId());
        Assert.assertNotNull(taskResponse);
        Assert.assertEquals(addTaskResponse.getId(), taskResponse.getId());
        Assert.assertEquals(addTaskResponse.getUserId(), taskResponse.getUserId());
        Assert.assertEquals(addTaskResponse.getTitle(), taskResponse.getTitle());
        Assert.assertEquals(addTaskResponse.getDescription(), taskResponse.getDescription());
    }

    @Test
    public void testGetByTitle() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Task task = CaseDataTaskProvider.createTask();
        Assert.assertNotNull(task);
        task.setUserId(user.getId());
        task.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Task addTaskResponse = this.userTaskService.addUserOwner(user.getId(), task.getTitle(), task.getDescription());
        Assert.assertNotNull(addTaskResponse);

        @Nullable final Task taskResponse = this.userTaskService.getUserOwnerByTitle(saveUserResponse.getId(), addTaskResponse.getTitle());
        Assert.assertNotNull(taskResponse);
        Assert.assertEquals(addTaskResponse.getId(), taskResponse.getId());
        Assert.assertEquals(addTaskResponse.getUserId(), taskResponse.getUserId());
        Assert.assertEquals(addTaskResponse.getTitle(), taskResponse.getTitle());
        Assert.assertEquals(addTaskResponse.getDescription(), taskResponse.getDescription());
    }

    @Test
    public void testGetAll() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        @NotNull final Task task = CaseDataTaskProvider.createTask();
        Assert.assertNotNull(task);
        task.setUserId(user.getId());
        task.setUser(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final Task addTaskResponse = this.userTaskService.addUserOwner(user.getId(), task.getTitle(), task.getDescription());
        Assert.assertNotNull(addTaskResponse);

        @Nullable final Collection<Task> tasksResponse = this.userTaskService.getUserOwnerAll(saveUserResponse.getId());
        Assert.assertNotNull(tasksResponse);
        Assert.assertNotEquals(0, tasksResponse.size());
        final boolean isUserTasks = tasksResponse.stream().allMatch(entity -> {
            assert saveUserResponse.getId() != null;
            return saveUserResponse.getId().equals(entity.getUserId());
        });
        Assert.assertTrue(isUserTasks);
    }

}