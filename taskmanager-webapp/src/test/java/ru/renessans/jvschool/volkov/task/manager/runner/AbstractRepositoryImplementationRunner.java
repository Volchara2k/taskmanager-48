package ru.renessans.jvschool.volkov.task.manager.runner;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.renessans.jvschool.volkov.task.manager.marker.RepositoryImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.ProjectRepositoryTest;
import ru.renessans.jvschool.volkov.task.manager.repository.RoleRepositoryTest;
import ru.renessans.jvschool.volkov.task.manager.repository.TaskRepositoryTest;
import ru.renessans.jvschool.volkov.task.manager.repository.UserRepositoryTest;

@RunWith(Categories.class)
@Categories.IncludeCategory(RepositoryImplementation.class)
@Suite.SuiteClasses(
        {
                ProjectRepositoryTest.class,
                RoleRepositoryTest.class,
                TaskRepositoryTest.class,
                UserRepositoryTest.class
        }
)
public abstract class AbstractRepositoryImplementationRunner {
}