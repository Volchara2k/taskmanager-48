package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.api.IEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IUserEndpoint;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.endpoint.InvalidEndpointException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.endpoint.InvalidEndpointsException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.endpoint.InvalidHostException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.endpoint.InvalidPortException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.UtilityImplementation;

import javax.xml.ws.Endpoint;
import java.util.Collection;
import java.util.Collections;

@Setter
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
public final class EndpointUtilTest {

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @Test(expected = InvalidEndpointException.class)
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    public void testNegativePublishObjectWithoutImplementor() {
        @NotNull final String host = "127.0.0.1";
        Assert.assertNotNull(host);
        @NotNull final Integer port = 9100;
        Assert.assertNotNull(port);
        EndpointUtil.publish((@Nullable IEndpoint) null, host, port);
    }

    @Test(expected = InvalidHostException.class)
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    public void testNegativePublishObjectWithoutHost() {
        @NotNull final Integer port = 9101;
        Assert.assertNotNull(port);
        EndpointUtil.publish(this.userEndpoint, "", port);
    }

    @Test(expected = InvalidPortException.class)
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    public void testNegativePublishObjectWithoutPort() {
        @NotNull final String host = "127.0.0.1";
        EndpointUtil.publish(this.userEndpoint, host, null);
    }

    @Test(expected = InvalidEndpointsException.class)
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    public void testNegativePublishObjectsWithoutImplementor() {
        @NotNull final String host = "127.0.0.1";
        Assert.assertNotNull(host);
        @NotNull final Integer port = 9102;
        Assert.assertNotNull(port);
        EndpointUtil.publish((Collection<IEndpoint>) null, host, port);
    }

    @Test(expected = InvalidHostException.class)
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    public void testNegativePublishObjectsWithoutHost() {
        @NotNull final Collection<IEndpoint> implementors = Collections.singletonList(this.userEndpoint);
        Assert.assertNotNull(implementors);
        @NotNull final Integer port = 9103;
        Assert.assertNotNull(port);
        EndpointUtil.publish(implementors, "   ", port);
    }

    @Test(expected = InvalidPortException.class)
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    public void testNegativePublishObjectsWithoutPort() {
        @NotNull final Collection<IEndpoint> implementors = Collections.singletonList(this.userEndpoint);
        Assert.assertNotNull(implementors);
        @NotNull final String host = "127.0.0.1";
        Assert.assertNotNull(host);
        EndpointUtil.publish(implementors, host, -100);
    }

    @Test
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    public void testPublishObject() {
        @NotNull final String host = "127.0.0.1";
        Assert.assertNotNull(host);
        @NotNull final Integer port = 9104;
        Assert.assertNotNull(port);
        @NotNull final Endpoint endpoint = EndpointUtil.publish(this.userEndpoint, host, port);
        Assert.assertNotNull(endpoint);
    }

    @Test
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    public void testPublishObjects() {
        @NotNull final Collection<IEndpoint> implementors = Collections.singletonList(this.userEndpoint);
        Assert.assertNotNull(implementors);
        @NotNull final String host = "127.0.0.1";
        Assert.assertNotNull(host);
        @NotNull final Integer port = 9105;
        Assert.assertNotNull(port);
        @NotNull final Collection<Endpoint> publishEndpoints = EndpointUtil.publish(implementors, host, port);
        Assert.assertNotNull(publishEndpoints);
        Assert.assertNotEquals(0, publishEndpoints.size());
    }

}