package ru.renessans.jvschool.volkov.task.manager.casedata;

@SuppressWarnings("unused")
public final class CaseDataSignatureUtilProvider {

    public Object[] invalidLinesCaseData() {
        return new Object[]{
                new Object[]{null, "null", 0},
                new Object[]{"", "null", 0},
                new Object[]{"    ", "null", 0}
        };
    }

    public Object[] invalidSaltsCaseData() {
        return new Object[]{
                new Object[]{"null", null, 0},
                new Object[]{"null", "", 0},
                new Object[]{"null", "    ", 0}
        };
    }

    public Object[] invalidCyclesCaseData() {
        return new Object[]{
                new Object[]{"null", "null", null},
                new Object[]{"null", "null", -1},
                new Object[]{"null", "null", -10}
        };
    }

    public Object[] validHashObjectsCaseData() {
        return new Object[]{
                new Object[]{"021a43fff51ada7660d77f6ed6bf436a", "null", "null", 1},
                new Object[]{"b99f754739f553c189557e318a305cd4", "    1", "    1", 2},
                new Object[]{"3ba94221340935ef40af5f3db326b5ef", "dwa       dwa", "dwa       dwa", 3},
                new Object[]{"f75728cbdfa214cb1c80b348867e417c", "1      ", "1      ", 4},
                new Object[]{"656397a1cc0bb3fbb8ffe0172e609702", "#!@  ", "#!@  ", 5}
        };
    }

    public Object[] validHashLinesCaseData() {
        return new Object[]{
                new Object[]{"946cafd578f628eba5b742ec27dd1e98", "null", "null", 1},
                new Object[]{"a83edf21e486e06babccfbab8d3fcf2a", "    1", "    1", 2},
                new Object[]{"b74c04cc141121d27659e77a53132295", "dwa       dwa", "dwa       dwa", 3},
                new Object[]{"44026bfa1889fc17becd4e152a3d0d30", "1      ", "1      ", 4},
                new Object[]{"39efa1d80378314bd1d500380275063e", "#!@  ", "#!@  ", 5}
        };
    }

}