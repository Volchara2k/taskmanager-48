package ru.renessans.jvschool.volkov.task.manager.runner;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.renessans.jvschool.volkov.task.manager.service.ConfigurationServiceTest;
import ru.renessans.jvschool.volkov.task.manager.service.DataInterChangeServiceTest;
import ru.renessans.jvschool.volkov.task.manager.service.DomainServiceTest;
import ru.renessans.jvschool.volkov.task.manager.service.SessionServiceTest;

@RunWith(Suite.class)
@Suite.SuiteClasses(
        {
                DataInterChangeServiceTest.class,
                DomainServiceTest.class,
                ConfigurationServiceTest.class,
                SessionServiceTest.class,
        }
)
public abstract class AbstractServiceTestRunner {
}