package ru.renessans.jvschool.volkov.task.manager.api.service.adapter;

import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.model.entity.Session;

public interface ISessionAdapterService extends IAdapterService<SessionDTO, Session> {
}