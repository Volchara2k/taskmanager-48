package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IUserEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.ISessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ISessionAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IUserLimitedAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.model.entity.Session;
import ru.renessans.jvschool.volkov.task.manager.model.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Objects;

@WebService
@Controller
@RequiredArgsConstructor
public final class UserEndpoint implements IUserEndpoint {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IUserLimitedAdapterService userLimitedAdapterService;

    @NotNull
    private final ISessionService sessionService;

    @NotNull
    private final ISessionAdapterService sessionAdapterService;

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @Override
    public UserLimitedDTO getUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final User user = this.userService.getUserById(current.getUserId());
        return this.userLimitedAdapterService.toDTO(user);
    }

    @WebMethod
    @WebResult(name = "userRole", partName = "userRole")
    @NotNull
    @Override
    public UserRoleType getUserRole(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable String userId = null;
        if (!Objects.isNull(sessionDTO)) userId = sessionDTO.getUserId();
        return this.userService.getUserRole(userId);
    }

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    @Override
    public UserLimitedDTO editProfile(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final User user = this.userService.editUserProfileById(current.getUserId(), firstName);
        return this.userLimitedAdapterService.toDTO(user);
    }

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    @Override
    public UserLimitedDTO editProfileWithLastName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName", partName = "lastName") @Nullable final String lastName
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final User user = this.userService.editUserProfileById(current.getUserId(), firstName, lastName);
        return this.userLimitedAdapterService.toDTO(user);
    }

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    @Override
    public UserLimitedDTO updatePassword(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final User user = this.userService.updateUserPasswordById(current.getUserId(), newPassword);
        return this.userLimitedAdapterService.toDTO(user);
    }

}