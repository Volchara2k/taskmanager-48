
package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteAllTasksResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteAllTasksResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deletedTasks" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteAllTasksResponse", propOrder = {
    "deletedTasks"
})
public class DeleteAllTasksResponse {

    protected int deletedTasks;

    /**
     * Gets the value of the deletedTasks property.
     * 
     */
    public int getDeletedTasks() {
        return deletedTasks;
    }

    /**
     * Sets the value of the deletedTasks property.
     * 
     */
    public void setDeletedTasks(int value) {
        this.deletedTasks = value;
    }

}
