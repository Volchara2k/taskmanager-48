package ru.renessans.jvschool.volkov.task.manager.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class ProjectUpdateByIndexListener extends AbstractProjectListener {

    @NotNull
    private static final String CMD_PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    @NotNull
    private static final String DESC_PROJECT_UPDATE_BY_INDEX = "обновить проект по индексу";

    @NotNull
    private static final String NOTIFY_PROJECT_UPDATE_BY_INDEX =
            "Происходит попытка инициализации обновления проекта. \n" +
                    "Для обновления проекта по индексу введите индекс проекта из списка.\n" +
                    "Для обновления проекта введите его заголовок или заголовок с описанием. ";

    public ProjectUpdateByIndexListener(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(projectEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_PROJECT_UPDATE_BY_INDEX;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_PROJECT_UPDATE_BY_INDEX;
    }

    @Async
    @Override
    @EventListener(condition = "@projectUpdateByIndexListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_PROJECT_UPDATE_BY_INDEX);
        @NotNull final Integer index = ViewUtil.getInteger() - 1;
        @NotNull final String title = ViewUtil.getLine();
        @NotNull final String description = ViewUtil.getLine();

        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        @Nullable final ProjectDTO update = super.projectEndpoint.updateProjectByIndex(current, index, title, description);
        ViewUtil.print(update);
    }

}