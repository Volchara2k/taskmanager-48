
package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteAllProjectsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteAllProjectsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deletedProjects" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteAllProjectsResponse", propOrder = {
    "deletedProjects"
})
public class DeleteAllProjectsResponse {

    protected int deletedProjects;

    /**
     * Gets the value of the deletedProjects property.
     * 
     */
    public int getDeletedProjects() {
        return deletedProjects;
    }

    /**
     * Sets the value of the deletedProjects property.
     * 
     */
    public void setDeletedProjects(int value) {
        this.deletedProjects = value;
    }

}
