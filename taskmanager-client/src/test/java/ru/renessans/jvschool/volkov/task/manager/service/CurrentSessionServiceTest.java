package ru.renessans.jvschool.volkov.task.manager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICurrentSessionRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataSessionProvider;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidSessionException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.CurrentSessionRepository;

@RunWith(JUnit4.class)
public final class CurrentSessionServiceTest {

    @NotNull
    private final ICurrentSessionRepository currentSessionRepository = new CurrentSessionRepository();

    @NotNull
    private final ICurrentSessionService currentSessionService = new CurrentSessionService(currentSessionRepository);

    @Before
    public void assertMainComponentsBefore() {
        Assert.assertNotNull(this.currentSessionRepository);
        Assert.assertNotNull(this.currentSessionService);
    }

    @Test(expected = InvalidSessionException.class)
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    public void testNegativeSubscribe() {
        this.currentSessionService.subscribe(null);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetSession() {
        @NotNull final SessionDTO sessionDTO = CaseDataSessionProvider.createSession();
        Assert.assertNotNull(sessionDTO);
        @NotNull final SessionDTO subscribeSession = this.currentSessionService.subscribe(sessionDTO);
        Assert.assertNotNull(subscribeSession);

        @Nullable final SessionDTO session = this.currentSessionService.getCurrentSession();
        Assert.assertNotNull(session);
        Assert.assertEquals(subscribeSession.getId(), session.getId());
        Assert.assertEquals(subscribeSession.getTimestamp(), session.getTimestamp());
        Assert.assertEquals(subscribeSession.getUserId(), session.getUserId());
        Assert.assertEquals(subscribeSession.getSignature(), session.getSignature());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testSubscribe() {
        @NotNull final SessionDTO sessionDTO = CaseDataSessionProvider.createSession();
        Assert.assertNotNull(sessionDTO);

        @NotNull final SessionDTO subscribeSession = this.currentSessionService.subscribe(sessionDTO);
        Assert.assertNotNull(subscribeSession);
        @Nullable final SessionDTO session = this.currentSessionService.getCurrentSession();
        Assert.assertNotNull(session);
        Assert.assertEquals(subscribeSession.getId(), session.getId());
        Assert.assertEquals(subscribeSession.getTimestamp(), session.getTimestamp());
        Assert.assertEquals(subscribeSession.getUserId(), session.getUserId());
        Assert.assertEquals(subscribeSession.getSignature(), session.getSignature());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testUnsubscribe() {
        @NotNull final SessionDTO sessionDTO = CaseDataSessionProvider.createSession();
        Assert.assertNotNull(sessionDTO);
        @NotNull final SessionDTO subscribeSession = this.currentSessionService.subscribe(sessionDTO);
        Assert.assertNotNull(subscribeSession);

        @Nullable final SessionDTO unsubscribeSession = this.currentSessionService.unsubscribe();
        Assert.assertNotNull(unsubscribeSession);
        Assert.assertEquals(subscribeSession.getId(), unsubscribeSession.getId());
        Assert.assertEquals(subscribeSession.getTimestamp(), unsubscribeSession.getTimestamp());
        Assert.assertEquals(subscribeSession.getUserId(), unsubscribeSession.getUserId());
        Assert.assertEquals(subscribeSession.getSignature(), unsubscribeSession.getSignature());
    }

}