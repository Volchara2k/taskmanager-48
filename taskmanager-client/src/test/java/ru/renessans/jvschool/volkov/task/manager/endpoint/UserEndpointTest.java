package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;

import java.util.UUID;

@Setter
@RunWith(value = SpringRunner.class)
@Category(IntegrationImplementation.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
public final class UserEndpointTest {

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @NotNull
    @Autowired
    private AuthenticationEndpoint authenticationEndpoint;

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @Before
    public void assertMainComponentsBefore() {
        Assert.assertNotNull(this.adminEndpoint);
        Assert.assertNotNull(this.authenticationEndpoint);
        Assert.assertNotNull(this.userEndpoint);
    }

    @Test
    public void testGetUser() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final UserLimitedDTO userResponse = this.userEndpoint.getUser(openSessionResponse);
        Assert.assertNotNull(userResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testGetUserRole() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final UserRole userRoleResponse = this.userEndpoint.getUserRole(openSessionResponse);
        Assert.assertNotNull(userRoleResponse);
        Assert.assertEquals(UserRole.ADMIN, userRoleResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testEditProfile() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_TEST_LOGIN, DemoDataConst.USER_TEST_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final UserLimitedDTO editUserResponse = this.userEndpoint.editProfile(
                openSessionResponse, "newFirstName"
        );
        Assert.assertNotNull(editUserResponse);
        Assert.assertEquals(DemoDataConst.USER_TEST_LOGIN, editUserResponse.getLogin());
        Assert.assertEquals("newFirstName", editUserResponse.getFirstName());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testEditProfileWithLastName() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final UserLimitedDTO editUserResponse = this.userEndpoint.editProfileWithLastName(
                openSessionResponse, "newData", "newData"
        );
        Assert.assertNotNull(editUserResponse);
        Assert.assertEquals(DemoDataConst.USER_DEFAULT_LOGIN, editUserResponse.getLogin());
        Assert.assertEquals("newData", editUserResponse.getFirstName());
        Assert.assertEquals("newData", editUserResponse.getLastName());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testUpdatePassword() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addUserResponse = this.authenticationEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addUserResponse);
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(login, password);
        Assert.assertNotNull(openSessionResponse);

        @Nullable final UserLimitedDTO updateUserResponse = this.userEndpoint.updatePassword(
                openSessionResponse, "newPassword"
        );
        Assert.assertNotNull(updateUserResponse);
        Assert.assertEquals(login, updateUserResponse.getLogin());
        Assert.assertEquals(addUserResponse.getId(), updateUserResponse.getId());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
        @NotNull final SessionDTO openAdminSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openAdminSessionResponse);
        final int deleteUserResponse = this.adminEndpoint.deleteUserByLogin(openAdminSessionResponse, login);
        Assert.assertEquals(1, deleteUserResponse);
        @Nullable final SessionDTO closeAdminSessionResponse = this.authenticationEndpoint.closeSession(openAdminSessionResponse);
        Assert.assertNotNull(closeAdminSessionResponse);
    }

}