package ru.renessans.jvschool.volkov.task.manager.util;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.UtilityImplementation;

@Category({PositiveImplementation.class, UtilityImplementation.class})
public final class SystemUtilTest {

    @Test
    public void testGetFormatData() {
        @NotNull final String hardwareDataAsString = SystemUtil.hardwareDataAsString();
        Assert.assertNotNull(hardwareDataAsString);
    }

}