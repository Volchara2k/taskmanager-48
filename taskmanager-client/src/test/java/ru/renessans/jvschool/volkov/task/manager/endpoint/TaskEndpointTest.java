package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;

import java.util.Collection;
import java.util.UUID;

@Setter
@RunWith(value = SpringRunner.class)
@Category(IntegrationImplementation.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
public final class TaskEndpointTest {

    @NotNull
    @Autowired
    private AuthenticationEndpoint authenticationEndpoint;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Before
    public void assertMainComponentsBeforeAll() {
        Assert.assertNotNull(this.authenticationEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
    }

    @Test
    public void testAddTask() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTaskResponse = this.taskEndpoint.addTask(
                openSessionResponse, title, description
        );
        Assert.assertNotNull(addTaskResponse);
        Assert.assertEquals(title, addTaskResponse.getTitle());
        Assert.assertEquals(description, addTaskResponse.getDescription());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testUpdateTaskByIndex() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(newDescription);
        @Nullable final TaskDTO updateTaskResponse = this.taskEndpoint.updateTaskByIndex(
                openSessionResponse, 0, title, description
        );
        Assert.assertNotNull(updateTaskResponse);
        Assert.assertEquals(title, updateTaskResponse.getTitle());
        Assert.assertEquals(description, updateTaskResponse.getDescription());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testUpdateTaskById() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final TaskDTO taskResponse = this.taskEndpoint.getTaskByIndex(openSessionResponse, 0);
        Assert.assertNotNull(taskResponse);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(newDescription);
        @Nullable final TaskDTO updateTaskResponse = this.taskEndpoint.updateTaskById(
                openSessionResponse, taskResponse.getId(), title, description
        );
        Assert.assertNotNull(updateTaskResponse);
        Assert.assertEquals(taskResponse.getId(), updateTaskResponse.getId());
        Assert.assertEquals(taskResponse.getUserId(), updateTaskResponse.getUserId());
        Assert.assertEquals(title, updateTaskResponse.getTitle());
        Assert.assertEquals(description, updateTaskResponse.getDescription());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testDeleteTaskById() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_TEST_LOGIN, DemoDataConst.USER_TEST_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTaskResponse = this.taskEndpoint.addTask(openSessionResponse, title, description);
        Assert.assertNotNull(addTaskResponse);

        final int deleteTaskResponse = this.taskEndpoint.deleteTaskById(openSessionResponse, addTaskResponse.getId());
        Assert.assertEquals(1, deleteTaskResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testDeleteTaskByIndex() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTaskResponse = this.taskEndpoint.addTask(openSessionResponse, title, description);
        Assert.assertNotNull(addTaskResponse);
        @NotNull final TaskDTO taskResponse = this.taskEndpoint.getTaskByIndex(openSessionResponse, 0);
        Assert.assertNotNull(taskResponse);

        final int deleteTaskResponse = this.taskEndpoint.deleteTaskByIndex(openSessionResponse, 0);
        Assert.assertEquals(1, deleteTaskResponse);
        @Nullable final SessionDTO closeSessionResponse = authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testDeleteTaskByTitle() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTaskResponse = this.taskEndpoint.addTask(openSessionResponse, title, description);
        Assert.assertNotNull(addTaskResponse);

        final int deleteTaskResponse = this.taskEndpoint.deleteTaskByTitle(openSessionResponse, addTaskResponse.getTitle());
        Assert.assertEquals(1, deleteTaskResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testDeleteAllTasks() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTaskResponse = this.taskEndpoint.addTask(openSessionResponse, title, description);
        Assert.assertNotNull(addTaskResponse);

        final int deleteTaskResponse = this.taskEndpoint.deleteAllTasks(openSessionResponse);
        Assert.assertNotEquals(0, deleteTaskResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testGetTaskById() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTaskResponse = this.taskEndpoint.addTask(openSessionResponse, title, description);
        Assert.assertNotNull(addTaskResponse);

        @Nullable final TaskDTO taskResponse = this.taskEndpoint.getTaskById(openSessionResponse, addTaskResponse.getId());
        Assert.assertNotNull(taskResponse);
        Assert.assertEquals(addTaskResponse.getId(), taskResponse.getId());
        Assert.assertEquals(addTaskResponse.getUserId(), taskResponse.getUserId());
        Assert.assertEquals(addTaskResponse.getTitle(), taskResponse.getTitle());
        Assert.assertEquals(addTaskResponse.getDescription(), taskResponse.getDescription());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testGetTaskByIndex() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final TaskDTO taskResponse = this.taskEndpoint.getTaskByIndex(openSessionResponse, 0);
        Assert.assertNotNull(taskResponse);
        Assert.assertEquals(openSessionResponse.getUserId(), taskResponse.getUserId());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testGetTaskByTitle() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTaskResponse = this.taskEndpoint.addTask(openSessionResponse, title, description);
        Assert.assertNotNull(addTaskResponse);

        @Nullable final TaskDTO taskResponse = this.taskEndpoint.getTaskByTitle(openSessionResponse, addTaskResponse.getTitle());
        Assert.assertNotNull(taskResponse);
        Assert.assertEquals(addTaskResponse.getId(), taskResponse.getId());
        Assert.assertEquals(addTaskResponse.getUserId(), taskResponse.getUserId());
        Assert.assertEquals(addTaskResponse.getTitle(), taskResponse.getTitle());
        Assert.assertEquals(addTaskResponse.getDescription(), taskResponse.getDescription());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testGetAllTasks() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final Collection<TaskDTO> allTasksResponse = this.taskEndpoint.getAllTasks(openSessionResponse);
        Assert.assertNotNull(allTasksResponse);
        Assert.assertNotEquals(0, allTasksResponse.size());
        final boolean isUserTasks = allTasksResponse.stream().allMatch(
                entity -> openSessionResponse.getUserId().equals(entity.getUserId()));
        Assert.assertTrue(isUserTasks);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

}