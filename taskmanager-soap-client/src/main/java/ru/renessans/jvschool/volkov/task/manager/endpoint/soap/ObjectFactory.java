
package ru.renessans.jvschool.volkov.task.manager.endpoint.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.renessans.jvschool.volkov.task.manager.endpoint.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AddProject_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "addProject");
    private final static QName _AddProjectResponse_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "addProjectResponse");
    private final static QName _DeleteProjectById_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "deleteProjectById");
    private final static QName _DeleteProjectByIdResponse_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "deleteProjectByIdResponse");
    private final static QName _GetAllProjects_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "getAllProjects");
    private final static QName _GetAllProjectsResponse_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "getAllProjectsResponse");
    private final static QName _GetProjectById_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "getProjectById");
    private final static QName _GetProjectByIdResponse_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "getProjectByIdResponse");
    private final static QName _UpdateProject_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "updateProject");
    private final static QName _UpdateProjectResponse_QNAME = new QName("http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", "updateProjectResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.renessans.jvschool.volkov.task.manager.endpoint.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddProject }
     * 
     */
    public AddProject createAddProject() {
        return new AddProject();
    }

    /**
     * Create an instance of {@link AddProjectResponse }
     * 
     */
    public AddProjectResponse createAddProjectResponse() {
        return new AddProjectResponse();
    }

    /**
     * Create an instance of {@link DeleteProjectById }
     * 
     */
    public DeleteProjectById createDeleteProjectById() {
        return new DeleteProjectById();
    }

    /**
     * Create an instance of {@link DeleteProjectByIdResponse }
     * 
     */
    public DeleteProjectByIdResponse createDeleteProjectByIdResponse() {
        return new DeleteProjectByIdResponse();
    }

    /**
     * Create an instance of {@link GetAllProjects }
     * 
     */
    public GetAllProjects createGetAllProjects() {
        return new GetAllProjects();
    }

    /**
     * Create an instance of {@link GetAllProjectsResponse }
     * 
     */
    public GetAllProjectsResponse createGetAllProjectsResponse() {
        return new GetAllProjectsResponse();
    }

    /**
     * Create an instance of {@link GetProjectById }
     * 
     */
    public GetProjectById createGetProjectById() {
        return new GetProjectById();
    }

    /**
     * Create an instance of {@link GetProjectByIdResponse }
     * 
     */
    public GetProjectByIdResponse createGetProjectByIdResponse() {
        return new GetProjectByIdResponse();
    }

    /**
     * Create an instance of {@link UpdateProject }
     * 
     */
    public UpdateProject createUpdateProject() {
        return new UpdateProject();
    }

    /**
     * Create an instance of {@link UpdateProjectResponse }
     * 
     */
    public UpdateProjectResponse createUpdateProjectResponse() {
        return new UpdateProjectResponse();
    }

    /**
     * Create an instance of {@link ProjectDTO }
     * 
     */
    public ProjectDTO createProjectDTO() {
        return new ProjectDTO();
    }

    /**
     * Create an instance of {@link TimeFrameDTO }
     * 
     */
    public TimeFrameDTO createTimeFrameDTO() {
        return new TimeFrameDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddProject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "addProject")
    public JAXBElement<AddProject> createAddProject(AddProject value) {
        return new JAXBElement<AddProject>(_AddProject_QNAME, AddProject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddProjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "addProjectResponse")
    public JAXBElement<AddProjectResponse> createAddProjectResponse(AddProjectResponse value) {
        return new JAXBElement<AddProjectResponse>(_AddProjectResponse_QNAME, AddProjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteProjectById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "deleteProjectById")
    public JAXBElement<DeleteProjectById> createDeleteProjectById(DeleteProjectById value) {
        return new JAXBElement<DeleteProjectById>(_DeleteProjectById_QNAME, DeleteProjectById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteProjectByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "deleteProjectByIdResponse")
    public JAXBElement<DeleteProjectByIdResponse> createDeleteProjectByIdResponse(DeleteProjectByIdResponse value) {
        return new JAXBElement<DeleteProjectByIdResponse>(_DeleteProjectByIdResponse_QNAME, DeleteProjectByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllProjects }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "getAllProjects")
    public JAXBElement<GetAllProjects> createGetAllProjects(GetAllProjects value) {
        return new JAXBElement<GetAllProjects>(_GetAllProjects_QNAME, GetAllProjects.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllProjectsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "getAllProjectsResponse")
    public JAXBElement<GetAllProjectsResponse> createGetAllProjectsResponse(GetAllProjectsResponse value) {
        return new JAXBElement<GetAllProjectsResponse>(_GetAllProjectsResponse_QNAME, GetAllProjectsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProjectById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "getProjectById")
    public JAXBElement<GetProjectById> createGetProjectById(GetProjectById value) {
        return new JAXBElement<GetProjectById>(_GetProjectById_QNAME, GetProjectById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProjectByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "getProjectByIdResponse")
    public JAXBElement<GetProjectByIdResponse> createGetProjectByIdResponse(GetProjectByIdResponse value) {
        return new JAXBElement<GetProjectByIdResponse>(_GetProjectByIdResponse_QNAME, GetProjectByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "updateProject")
    public JAXBElement<UpdateProject> createUpdateProject(UpdateProject value) {
        return new JAXBElement<UpdateProject>(_UpdateProject_QNAME, UpdateProject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "updateProjectResponse")
    public JAXBElement<UpdateProjectResponse> createUpdateProjectResponse(UpdateProjectResponse value) {
        return new JAXBElement<UpdateProjectResponse>(_UpdateProjectResponse_QNAME, UpdateProjectResponse.class, null, value);
    }

}
