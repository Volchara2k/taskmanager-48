package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public final class SystemUtil {

    @NotNull
    private static final String HARDWARE_DATA_PATTERN =
            "Доступные процессоры (ядра): %d; \n" +
                    "Свободная память: %s; \n" +
                    "Максимальная память: %s; \n" +
                    "Общая память, доступная JVM: %s; \n" +
                    "Используемая память JVM: %s.";

    @NotNull
    public String getHardwareDataAsString() {
        final int availableProcessors = RuntimeUtil.getAvailableProcessors();
        final long freeMemory = RuntimeUtil.getFreeMemory();
        @NotNull final String freeMemoryFormat = ConversionUtil.asReadableStingFrom(freeMemory);

        final long totalMemory = RuntimeUtil.getTotalMemory();
        @NotNull final String totalMemoryFormat = ConversionUtil.asReadableStingFrom(totalMemory);

        final long usedMemory = totalMemory - freeMemory;
        @NotNull final String usedMemoryFormat = ConversionUtil.asReadableStingFrom(usedMemory);

        final long maxMemory = RuntimeUtil.getMaxMemory();
        @NotNull final String maxMemoryValue = ConversionUtil.asReadableStingFrom(maxMemory);
        @NotNull final String maxMemoryFormat = (maxMemory == Long.MAX_VALUE ? "без ограничений" : maxMemoryValue);

        return String.format(
                HARDWARE_DATA_PATTERN,
                availableProcessors, freeMemoryFormat, maxMemoryFormat, totalMemoryFormat, usedMemoryFormat
        );
    }


}