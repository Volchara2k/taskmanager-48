
package ru.renessans.jvschool.volkov.task.manager.endpoint.soap;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for userOwnerStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="userOwnerStatus"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NOT_STARTED"/&gt;
 *     &lt;enumeration value="IN_PROGRESS"/&gt;
 *     &lt;enumeration value="COMPLETED"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "userOwnerStatus")
@XmlEnum
public enum UserOwnerStatus {

    NOT_STARTED,
    IN_PROGRESS,
    COMPLETED;

    public String value() {
        return name();
    }

    public static UserOwnerStatus fromValue(String v) {
        return valueOf(v);
    }

}
