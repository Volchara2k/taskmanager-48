package ru.renessans.jvschool.volkov.task.manager.listener.security;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICookieService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.AuthenticationSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;

@RequiredArgsConstructor
public abstract class AbstractSecurityListener extends AbstractListener {

    @NotNull
    protected final AuthenticationSoapEndpoint authenticationEndpoint;

    @NotNull
    protected final ICookieService sessionService;

}