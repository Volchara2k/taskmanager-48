<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<jsp:include page="../include/_header.jsp"/>

<div class="main_header-content">
    <sec:authorize access="isAuthenticated()">
        <h1>
            Добро пожаловать, <b><sec:authentication property="name"/></b>, в приложение &laquo;Менеджер задач&raquo;!
        </h1>
    </sec:authorize>

    <sec:authorize access="!isAuthenticated()">
        <h1>
            Добро пожаловать в приложение &laquo;Менеджер задач&raquo;!
        </h1>
    </sec:authorize>
</div>

<jsp:include page="../include/_footer.jsp"/>