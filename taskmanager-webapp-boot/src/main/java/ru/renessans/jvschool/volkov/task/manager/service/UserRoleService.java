package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserRoleService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.entity.User;
import ru.renessans.jvschool.volkov.task.manager.entity.UserRole;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserRoleException;
import ru.renessans.jvschool.volkov.task.manager.repository.IUserRoleRepository;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Service
@Transactional
public class UserRoleService extends AbstractService<UserRole> implements IUserRoleService {

    @NotNull
    private final IUserService userService;

    @Lazy
    public UserRoleService(
            @NotNull final IUserRoleRepository roleRepository,
            @NotNull final IUserService userService
    ) {
        super(roleRepository);
        this.userService = userService;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Set<UserRole> addRole(
            @Nullable final String userId,
            @Nullable final Collection<UserRoleType> userRoleTypes
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(userRoleTypes)) throw new InvalidUserRoleException();

        @Nullable final User user = this.userService.getUserById(userId);
        if (Objects.isNull(user)) throw new InvalidUserException();

        @NotNull final Set<UserRole> userRoles = new HashSet<>();
        userRoleTypes.forEach(userRoleType -> {
            @NotNull final UserRole userRole = new UserRole(user, userRoleType);
            userRoles.add(super.save(userRole));
        });

        return userRoles;
    }

}