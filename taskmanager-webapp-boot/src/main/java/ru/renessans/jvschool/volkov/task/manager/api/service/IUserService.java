package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.entity.User;
import ru.renessans.jvschool.volkov.task.manager.entity.UserRole;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;

import java.util.Collection;
import java.util.Set;

public interface IUserService extends IService<User> {

    @NotNull
    User addUser(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    User addUser(
            @Nullable String login,
            @Nullable String password,
            @Nullable String firstName
    );

    @NotNull
    User addUser(
            @Nullable String login,
            @Nullable String password,
            @Nullable Set<UserRoleType> userRoleTypes
    );

    @NotNull
    User addUser(
            @Nullable User tempUser,
            @Nullable Set<UserRoleType> userRoleTypes
    );

    @Nullable
    User getUserById(
            @Nullable String id
    );

    @Nullable
    User getUserByLogin(
            @Nullable String login
    );

    boolean existsUserByLogin(
            @Nullable String login
    );

    @Nullable
    Set<UserRole> getUserRoles(
            @Nullable String userId
    );

    @Nullable
    User editUserProfileById(
            @Nullable String id,
            @Nullable String firstName
    );

    @Nullable
    User editUserProfileById(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName
    );

    @Nullable
    User updateUserPasswordById(
            @Nullable String id,
            @Nullable String newPassword
    );

    @Nullable
    User lockUserByLogin(
            @Nullable String login
    );

    @Nullable
    User unlockUserByLogin(
            @Nullable String login
    );

    int deleteUserById(
            @Nullable String id
    );

    int deleteUserByLogin(
            @Nullable String login
    );

    @NotNull
    Collection<User> initialDemoUsers();

}