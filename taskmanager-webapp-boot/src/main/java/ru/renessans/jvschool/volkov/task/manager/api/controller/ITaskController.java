package ru.renessans.jvschool.volkov.task.manager.api.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.renessans.jvschool.volkov.task.manager.dto.SecureUserDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;

public interface ITaskController {

    @NotNull
    @GetMapping("/tasks")
    ModelAndView index(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO
    );

    @NotNull
    @GetMapping("/task/create")
    ModelAndView create(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO
    );

    @NotNull
    @PostMapping("/task/create")
    ModelAndView create(
            @ModelAttribute("task") @NotNull TaskDTO taskDTO,
            @NotNull BindingResult result
    );

    @NotNull
    @GetMapping("/task/view/{id}")
    ModelAndView view(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO,
            @PathVariable("id") @NotNull String id
    );

    @NotNull
    @GetMapping("/task/delete/{id}")
    ModelAndView delete(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO,
            @PathVariable("id") @NotNull String id
    );

    @NotNull
    @GetMapping("/task/edit/{id}")
    ModelAndView edit(
            @AuthenticationPrincipal @NotNull SecureUserDTO userDTO,
            @PathVariable("id") @NotNull String id
    );

    @NotNull
    @PostMapping("/task/edit/{id}")
    ModelAndView edit(
            @ModelAttribute("task") @NotNull TaskDTO taskDTO,
            @NotNull BindingResult result
    );

}