package ru.renessans.jvschool.volkov.task.manager;

import org.jetbrains.annotations.Nullable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class WebApplication {

	public static void main(@Nullable final String[] args) {
		SpringApplication.run(WebApplication.class, args);
	}

}