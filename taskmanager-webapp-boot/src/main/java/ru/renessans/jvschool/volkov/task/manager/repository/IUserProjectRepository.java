package ru.renessans.jvschool.volkov.task.manager.repository;

import ru.renessans.jvschool.volkov.task.manager.entity.Project;

public interface IUserProjectRepository extends IUserOwnerRepository<Project> {
}